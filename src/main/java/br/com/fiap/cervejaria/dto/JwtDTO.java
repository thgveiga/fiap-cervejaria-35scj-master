package br.com.fiap.cervejaria.dto;

public class JwtDTO {

    private String token;

    public void setToken(String token) {
      this.token = token;
    }

    public String getToken() {
       return token;
    }
}
