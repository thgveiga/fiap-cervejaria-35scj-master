package br.com.fiap.cervejaria.service.impl;

import br.com.fiap.cervejaria.dto.AuthDTO;
import br.com.fiap.cervejaria.dto.CreateUserDTO;
import br.com.fiap.cervejaria.dto.JwtDTO;
import br.com.fiap.cervejaria.dto.UserDTO;
import br.com.fiap.cervejaria.entity.User;
import br.com.fiap.cervejaria.repository.UserRepository;
import br.com.fiap.cervejaria.security.JwtTokenUtil;
import br.com.fiap.cervejaria.service.UserService;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    public UserServiceImpl(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO create(CreateUserDTO createUserDTO) {
        User user = new User();
        user.setUsername(createUserDTO.getUsername());
        user.setPassword(createUserDTO.getPassword());

        User savedUser = userRepository.save(user);

        return new UserDTO(savedUser);
    }

    @Override
    public JwtDTO login(AuthDTO authDTO) {
        try{
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
               authDTO.getUsername(), authDTO.getPassword()
            ));

        }catch (DisabledException  disabledException){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "user.disabled");

        }catch (BadCredentialsException badCredentialsException){
            throw  new ResponseStatusException(HttpStatus.BAD_REQUEST, "invalid.credentials");
        }

        String token = jwtTokenUtil.generateToken(authDTO.getUsername());
        JwtDTO jwtDTO = new JwtDTO();
        jwtDTO.setToken(token);
        return jwtDTO;
    }

}
